module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["links.papareact.com", "image.tmdb.org", "images.unsplash.com", "www.facebook.com", "www.instagram.com"]
  }
}
