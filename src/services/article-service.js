import axios from "axios";
import { getSession } from "next-auth/react";
import api from "./token-interceptors";

export async function fetchArticles() {
    const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article')
    return response.data
}

export async function fetchArticleById(id) {
    const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article' + id)
    return response.data
}

export async function postArticleComment(id, comment) {
    const session = await getSession()
    const user = session?.user
    console.log(id, comment)
    const response= await api.post(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article/' + id + '/comment', 
    {description: comment})

    return response.data
}

export async function deleteArticleComment(id, comment) {
    const session = await getSession()
    const user = session?.user

    const response = await api.delete(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article/' + id + '/comment')

    return response.data
}

export async function addArticleFavorite(id) {
    const session = await getSession()
    const user = session?.user

    const response = await api.post(process.env.NEXT_PUBLIC_SERVER_URL + '/api/article/' + id + '/favoris')

    return response.data
}

export async function getCategory(id) {
    const resp = await api.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/category/" + id);
    return resp.data
}