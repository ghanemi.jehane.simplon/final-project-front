import axios from "axios";
import { getSession } from "next-auth/react";
import Router from 'next/router'

const api = axios.create({
	baseURL: process.env.NEXT_PUBLIC_SERVER_URL
})
export const apiFetcher = (url) => api.get(url).then(res => res?.data ? res.data : null)

api.interceptors.response.use((response) => {

	return response
}, (error) => {
	
})
api.interceptors.request.use(async (config) => {
	
	const session = await getSession();
	if (session && typeof window !== "undefined") {

		config.headers.authorization = 'bearer ' + session.accessToken;
	}
	return config;
});



export default api
