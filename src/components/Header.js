import React from 'react'
import HeaderItem from './HeaderItem';
import Image from 'next/image';
import { signIn, signOut, useSession } from "next-auth/react"
import { AdjustmentsIcon, BadgeCheckIcon, CollectionIcon, HomeIcon, LightningBoltIcon, LoginIcon, LogoutIcon, PlusIcon, SaveIcon, SearchIcon, UserIcon, } from '@heroicons/react/outline'
import Link from 'next/link';


function Header() {
    const { data: session } = useSession()
    const user = session?.user;
    return (
        <div>
            <header className="flex flex-col sm:flex-row m-5 justify-between items-center h-auto">
                <Link href='/' className="cursor-pointer">
                    <div>
                        <Image src="/Logo.png"
                            width={300} height={150}
                            className="object-contain cursor-pointer" />
                    </div>
                </Link>
                <div className="flex flex-grow justify-evenly max-w-2xl">
                   <Link href='/'>
                    <HeaderItem title='HOME' Icon={HomeIcon} />
                    </Link>
                    
                    <a href="/category">
                    <HeaderItem title='CATEGORIES' Icon={CollectionIcon} />
                    </a>
                    <HeaderItem title='SEARCH' Icon={SearchIcon} />
                    {!session &&
                        <div className="flex">
                            <div onClick={() => signIn()}>
                                <HeaderItem title='SIGN IN' Icon={LoginIcon} />
                            </div>
                        </div>



                    }
                    {session, user &&
                    
                        <div className="flex">
                          
{user.photo  ?  <div className="flex flex-col items-center cursor-pointer 
                            w-12 sm:w-20 hover:text-white group">
                                <a href="/user/account" >
                                    
                                    <img className='rounded-full object-cover w-10 h-10 mb-1 group-hover:animate-bounce' src={user.photo } />
                                    <p className="opacity-0 group-hover:opacity-100 tracking-widest uppercase">{ user.username}</p>

                                </a>
                            </div> :
                              <a href="/user/account">
                              <HeaderItem title={user.username} Icon={UserIcon} />
                          </a>
                            }
                         

                            <div onClick={() => signOut()}>
                                <HeaderItem title='LOGOUT' Icon={LogoutIcon} />
                            </div>





                        </div>
                    }

                    {session, user?.role == "admin" &&
                    <a href="/admin">
                        <HeaderItem title='ADD' Icon={AdjustmentsIcon} />
                    </a>
                    }

                </div>


            </header>
        </div>
    )
}

export default Header



