import React, {Fragment, useRef, useState} from 'react'
import Image from 'next/image'
import Link from 'next/link'
import moment from 'moment'
import Rating from './Rating'
import { ExclamationIcon, HeartIcon, TrashIcon } from '@heroicons/react/solid'
import { useSession } from 'next-auth/react'
import { addArticleFavorite } from '../services/article-service'
import { Dialog, Transition } from '@headlessui/react'

function Article({ id, title, description, image, date, rating, comments, onDelete }) {
    const { data: session } = useSession()
    const user = session?.user
    let [isOpen, setIsOpen] = useState(false)
    const cancelButtonRef = useRef(null)

    function closeModal() {
        setIsOpen(false)
    }

    function openModal() {
        setIsOpen(true)
    }

    async function addFavorites(data) {
        const resp = await addArticleFavorite(data)
    }


    return (
        <div className="p-2 transition duration-200 ease-in transform sm:hover:scale-105 
        hover:z-9 group cursor-pointer px-5 my-10">
            <Link href={"/article/" + id}>


                <Image layout="responsive" className="rounded-md" width={1920} height={1000} src={image} />

            </Link>
            <div className="p-2 space-y-2">
                <h1 className="mt-1 text-2xl text-white transition-all duration-100 ease-in-out group-hover:font-bold">{title}</h1>
                <p className="">{description}</p>

                <p className="flex items-center">{moment(date).format('L')}</p>

                <div className="flex h-7 justify-between">
                    <Rating />
                    <HeartIcon color={user?.favorites.find(item => item.id === id) ? "red" : "white"} onClick={() => addFavorites(id)} className="hover:scale-125 cursor-pointer transition-all duration-150 ease-out " />
                </div>
                {session, user?.role === "admin" &&

                    <TrashIcon className="h-7 flex cursor-pointer  " onClick={openModal} />
                  

                }

      <Dialog open={isOpen} as="div" className="fixed z-10 inset-0 overflow-y-auto" initialFocus={cancelButtonRef} onClose={closeModal}>
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
  
            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />

          {/* This element is to trick the browser into centering the modal contents. */}
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
            &#8203;
          </span>
          
            <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="sm:flex sm:items-start">
                  <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                    <ExclamationIcon className="h-6 w-6 text-red-600" aria-hidden="true" />
                  </div>
                  <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                    <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
                      Supprimer un article
                    </Dialog.Title>
                    <div className="mt-2">
                      <p className="text-sm text-gray-500 ">
                        Etes-vous sûr de vouloir supprimer cet article ?
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                <button
                  type="button"
                  className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm"
                  onClick={() => onDelete(id)}>
                  Supprimer
                </button>
                <button
                  type="button"
                  className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                  onClick={closeModal}
                  ref={cancelButtonRef}
                >
                  Annuler
                </button>
              </div>
            </div>

        </div>
      </Dialog>


            </div>




        </div>

    )
}

export default Article
