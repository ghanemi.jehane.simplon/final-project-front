import React from 'react'
import Article from './Article'


function ArticleFeed({articles, onDelete}) {
    return (
        <div className="grid sm:grid md:grid-cols-2 xl:grid-cols-3">
            {articles.map(({id,title,description,duration,trailer,image,categoriesId})=>(
                <Article
                key={id}
                id={id}
                title={title}
                description={description}
                duration={duration}
                trailer={trailer}
                image={image}
                categoriesId={categoriesId}
                onDelete={onDelete}
                />
            ))}
        </div>

    )
}

export default ArticleFeed