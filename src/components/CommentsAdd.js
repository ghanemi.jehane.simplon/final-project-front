import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import React, { useState } from 'react'
import { useForm } from 'react-hook-form';
import { postArticleComment } from '../services/article-service';

function CommentsAdd({id}) {
    const {data: session} = useSession();
    const user = session?.user
    const router = useRouter();
    const {register, handleSubmit} = useForm();

    const refreshData = () => {
        router.replace(router.asPath);
    }

    async function onSubmit(data) {
        
         await postArticleComment(id, data.comment)
        
        refreshData()
    }

    if (session) {
    return (
        
        <div className="flex ml-10 justify-start items-center">
            
            <div className="w-1/2 p-2 pt-4 rounded shadow-lg">
               <div className="flex ml-3">
               <div className="mr-3">
                <img src={user?.photo} className="rounded-full object-cover h-12 w-12 "/>
                </div>
                <div>
                    <h1 className="font-semibold text-lg text-gray-500 capitalize">{user?.username}</h1>
                    <p className="text-sm text-gray-500">2 seconds ago</p>
                </div>
               
               </div>
               <form onSubmit={handleSubmit(onSubmit)} className="mt-3 p-3 w-full">
                <div>
                    <textarea placeholder="Leave a comment..." className="text-black border p-2 rounded w-full"{...register ("comment", {})} />
                </div>

<div className="flex justify-between mx-3 ">
    <input className="cursor-pointer px-4 py-1 bg-gray-500 text-white rounded font-light "  type="submit" />
</div>
            </form>

            </div>
            
       
        
        </div>
    )
    }
    if (!session) {
        return(
            <div>
                <p>You have to login to comment</p>
            </div>
        )
    }
  
}

export default CommentsAdd
