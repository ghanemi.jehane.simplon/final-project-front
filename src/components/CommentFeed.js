import React from 'react'
import CommentInput from './CommentInput'

function CommentFeed({ comments }) {
    return (
        <div>
        {comments.map(({id, userId, articleId, description, date})=>(
          <CommentInput
          key={id}
          id={id}
          userId={userId}
          description={description}
          articleId={articleId}
          date={date}
          
          />
        ))}
  </div>
      
    )
}

export default CommentFeed
