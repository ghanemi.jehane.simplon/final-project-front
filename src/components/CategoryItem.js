import React from 'react'

function CategoryItem({id, label}) {
    return (
        
            <div class=" p-6 border-2 border-black hover:bg-black rounded-xl hover:text-white">
            <p className="text-4xl uppercase font-semibold justify-center text-center my-8">{label}</p>
            </div>
        
    )
}

export default CategoryItem
