import { useSession } from 'next-auth/react'
import React, { useState } from 'react'

function PostForm({ onSubmit }) {
    const {data: session} = useSession();
    const user = session?.user;

    const initial = {
        title : '',
        description: '',
        image: '',
        categoriesId: ''

    }

    const [form, setForm] = useState(initial)
    console.log(form);
    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(form);
    }


    const handleFile = async (event)=>{
        let target = event.target 
        let name = target.name;
        let value = target.files?.item(0);
        let change = { ...form, [name]: value }
  
        setForm(change)
    }

    console.log
    return (
        <>
        {session, user?.role == "admin" &&







        <div className="flex flex-col lg:flex-row justify-around items-center border-b border-black">
            <h1 className="text-xl mt-5">Formulaire d'ajout</h1>
            <div className="p-2 mb-5">
                <form className="mt-5" onSubmit={handleSubmit}>
                    <div className="border border-gray-300 p-2 text-black">
                        <input type="text" placeholder="title" onChange={handleChange} name="title" value={form.title}  />

                    </div>
                    <div className="border border-gray-300 p-2 text-black">
                        <input type="text" placeholder="description" onChange={handleChange} name="description" value={form.description} />

                    </div>
                    {/* <div className="border border-gray-300 p-2 text-black">
                        <input type="text" placeholder="image" onChange={handleChange} name="image" value={form.image}  />

                    </div> */}
                    {/* <div className="border border-gray-300 p-2 text-black">
                    <input type="file" className="form-control-file" name="image" onChange={handleFile} value={form.image}/>
                    </div> */}
                    <div className="form-group border border-gray-300 p-2 text-black">
                    <label htmlFor="formFile" className="form-label mt-4">Photo</label>
                    <input onChange={handleFile} name="image"  className="form-control" type="file" id="formFile" />
                  </div>
                   
                    <div className="border border-gray-300 p-2 text-black">
                        <input type="text" placeholder="categoriesId" onChange={handleChange} name="categoriesId" value={form.categoriesId}  />

                    </div>
                    <div className="border bg-blue-600 text-center p-2 text-black">
                        <button>Ajouter</button>

                    </div>
                </form>
            </div>
            
        </div>
}
        </>
    )
}

export default PostForm
