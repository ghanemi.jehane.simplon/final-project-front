
import Link from 'next/link'
import NavItem from './NavItem'


function Nav({ categories }) {

    return (
        <nav className="relative">
<div className="flex px-10 sm:px-20 text-2xl whitespace-nowrap
        space-x-10 sm:space-x-20 overflow-x-scroll scrollbar-hide ">

            {categories.map(({id, label}) => (
                <div key={id} className="flex text-white">
                    <a href={"/category/" + id}>
                    <NavItem key={id} label={label}/>
                    </a>
                </div>
            )
                
            )}
{/* <div className="absolute top-0 right-0 bg-gradient-to-l from-[#06202A]
h-10 w-1/2">

</div> */}

        </div>
        </nav>
        
    )
}


export default Nav