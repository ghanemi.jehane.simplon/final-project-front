import React from 'react'

function Banner() {
    return (
        <div>
            <aside>
  <div class="px-4 py-16 mx-auto max-w-screen-2xl sm:px-6 lg:px-8">
    <div class="grid grid-cols-1 gap-4 sm:grid-cols-2 lg:grid-cols-4">
      <div class="p-8 text-center text-[#004058] bg-white rounded-lg sm:col-span-2 sm:p-16 lg:py-24">
        <div class="max-w-lg mx-auto space-y-8">
          <p class="text-3xl font-bold sm:text-4xl">
            Trouvez les meilleurs restaurants dans la ville de LYON !
          </p>

          <p class="text-sm">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio unde maiores natus placeat earum, atque
            necessitatibus sunt. Ducimus inventore qui unde, corporis itaque rerum illum tempore quod? Quae, quam
            quaerat.
          </p>

          <a
            href=""
            class="inline-flex items-center px-5 py-3 mt-8 font-medium text-indigo-600 bg-white rounded-lg hover:opacity-75"
          >
            Come and see

            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="flex-shrink-0 w-4 h-4 ml-3"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
            </svg>
          </a>
        </div>
      </div>

      <div class="relative h-64 lg:order-first lg:h-full">
        <img
          src="https://images.unsplash.com/photo-1482049016688-2d3e1b311543?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8Zm9vZHxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60"
          alt="Sunset with palm trees"
          class="absolute inset-0 object-cover w-full h-full rounded-lg"
        />
      </div>

      <div class="relative h-64 lg:h-full">
        <img
          src="https://images.unsplash.com/photo-1540189549336-e6e99c3679fe?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDR8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60"
          alt="Man in a hat and yellow jumper"
          class="absolute inset-0 object-cover w-full h-full rounded-lg"
        />
      </div>
    </div>
  </div>
</aside>
        </div>
    )
}

export default Banner
