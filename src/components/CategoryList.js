import React from 'react'
import CategoryItem from './CategoryItem'

function CategoryList({category}) {
    return (
        <div className="flex flex-wrap -mx-2 overflow-hidden sm:-mx-6 md:-mx-10 lg:-mx-10 xl:-mx-10 px-10
        ">
            {category.map(({ id, label }) => (
                <div className="my-2 px-2 w-full overflow-hidden sm:my-6 sm:px-6 md:my-1 md:px-1 md:w-full lg:my-4 lg:px-4 lg:w-1/2 xl:my-8 xl:px-8 xl:w-1/3
                ">
                <div class=" grid-row-2 gap-4 sm:grid-cols-2 md:grid-cols-4">
                    {/* <!-- Remove class [ h-24 ] when adding a card block --> */}
                    {/* <!-- Remove class [ border-gray-300  dark:border-gray-700 border-dashed border-2 ] to remove dotted border --> */}
                    <a href={"/category/" + id}>
                    <CategoryItem key={id} label={label} />
                    </a>
                </div>
                </div>
            ))}
        </div>
    )
}

export default CategoryList
