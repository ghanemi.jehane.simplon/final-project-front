import { useSession } from 'next-auth/react';
import React, { useState } from 'react'
import api from '../services/token-interceptors';

function AddArticle( {onSubmit} ) {
    const {data: session} = useSession();
    const user = session?.user;

    const initial = {
        title : '',
        description: '',
        image: '',
        categoriesId: ''

    }

    const [form, setForm] = useState(initial)
    console.log(form);
    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(form);
    }


    const handleFile = async (event)=>{
        let target = event.target 
        let name = target.name;
        let value = target.files?.item(0);
        let change = { ...form, [name]: value }
  
        setForm(change)
    }




    return (
        <div>
{session, user?.role == "admin" &&
<section class="bg-gray-200">
  <div class="max-w-screen-xl px-4 py-16 mx-auto sm:px-6 lg:px-8">
    <div class="grid grid-cols-1 gap-x-16 gap-y-8 lg:grid-cols-5">
      <div class="lg:py-12 lg:col-span-2">
        <h1 class="max-w-xl text-4xl text-black font-semibold mt-20">
         Ajouter un article
        </h1>

      
      </div>

      <div class="p-8 bg-[#004058] rounded-lg shadow-lg lg:p-12 lg:col-span-3">
        <form onSubmit={handleSubmit} class="space-y-4">
          <div>
            <label class="sr-only" for="title">Title</label>
            <input type="text" placeholder="Title" onChange={handleChange} name="title" value={form.title} class="w-full p-3 text-sm text-gray-600 border-gray-200 rounded-lg" />
          </div>
          <div>
            <label class="sr-only" for="image">Image</label>
            <input type="text" placeholder="Image" onChange={handleChange} name="image" value={form.image} class="w-full p-3 text-sm text-gray-600 border-gray-200 rounded-lg" />
          </div>

          <div class="grid grid-cols-1 gap-4 sm:grid-cols-2">
 

            <div>
              <label class="sr-only" for="category">Catégorie Id</label>
              <input class="w-full p-3 text-sm border-gray-200 rounded-lg text-black"     type="text" placeholder="categoriesId" onChange={handleChange} name="categoriesId" value={form.categoriesId}  />
           
            </div>
          </div>




          <div>
            <label class="sr-only" for="description">Description</label>
            <textarea
            onChange={handleChange}
              class="w-full p-3 text-sm border-gray-200 rounded-lg text-gray-600"
              placeholder="Description"
              name="description"
              value={form.description}
              rows="8"
              id="message"
            ></textarea>
          </div>

          <div class="mt-4">
            <button
              type="submit"
              class="inline-flex items-center justify-center w-full px-5 py-3 text-white bg-black rounded-lg sm:w-auto"
            >
              <span class="font-medium"> Poster </span>

              <svg
                xmlns="http://www.w3.org/2000/svg"
                class="w-5 h-5 ml-3"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
              </svg>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
}
        </div>
    )
}

export default AddArticle
