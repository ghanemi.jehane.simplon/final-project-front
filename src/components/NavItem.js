import React from 'react'

function NavItem({id, label}) {
    return (
        <div className="flex">
           
                
        <p className="last:pr-24 cursor-pointer transition duration-100
        transform hover:scale-125 hover:text-white active:text-red-800">{label}</p>

</div>

    )
}

export default NavItem
