import { useSession } from 'next-auth/react'
import React from 'react'


function CommentInput({id, userId, articleId, description, date, onDelete }) {
    const { data: session } = useSession()
    const user = session?.user
    return (
        <div>
            <div className="rounded-full w-10 h-10">
            <Image layout="fill" src={user?.photo} />
            </div>
            <p className="flex items-center">{moment(date).format('L')}</p>
            <p className="">{description}</p>
             <TrashIcon className="h-7 cursor-pointer" />
        </div>
    )
}

export default CommentInput
