import React, { useState } from 'react'
import { StarIcon } from '@heroicons/react/solid';
function Rating() {
    const [rating, setRating] = useState(0);
    const [hover, setHover] = useState(0);

    return (
        <div className="flex">
            {[...Array(5)].map((star, i) => {
                const ratingValue = i + 1;

                return(
                    <label >
                        <input className= 'hidden' type='radio' name='rating' value={ratingValue}
                        onClick={() => setRating(ratingValue)}/>
                        <StarIcon 
                        color={ratingValue <= (hover || rating) ? '#ffc107' : '#e4e5e9'}
                        onMouseOver = {() => setHover(ratingValue)}
                        onMouseLeave = {() => setHover(0)}
                        className= 'cursor-pointer h-5 star justify-between' />
                    </label>
                )
            })}
            <p className="mx-3">{rating}/5</p>
        </div>
    )
}

export default Rating

export async function getServerSideProps() {
    
}