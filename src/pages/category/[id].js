import moment from 'moment'
import { useSession } from 'next-auth/react'
import Image from 'next/image'
import React from 'react'
import ArticleFeed from '../../components/ArticleFeed'
import Header from '../../components/Header'
import { getCategory } from '../../services/article-service'
import Rating from '../../components/Rating'
import { HeartIcon, TrashIcon } from '@heroicons/react/solid'
import api from '../../services/token-interceptors'
import Nav from '../../components/Nav'

function OneCategory({ category, categories }) {
    const { data: session } = useSession()
    const user = session?.user
    async function addFavorites(data) {
        const resp = await addArticleFavorite(data)


    }
    return (
        <div>
            <Header/>
            <Nav categories={categories}/>
            <h1 className="text-center text-4xl font-semibold mt-10">{category.label}</h1>
            <div className="grid sm:grid md:grid-cols-2 xl:grid-cols-3">
                
            {category.articles.map((item) => {
                return(
                    <div>
                             <div className="p-2 transition duration-200 ease-in transform sm:hover:scale-105 
        hover:z-50 group cursor-pointer px-5 my-10">
            <a href={"/article/" + item.id}>


                <Image layout="responsive" className="rounded-md" width={1920} height={1000} src={item.image} />

            </a>
            <div className="p-2 space-y-2">
                <h1 className="mt-1 text-2xl text-white transition-all duration-100 ease-in-out group-hover:font-bold">{item.title}</h1>
                <p className="">{item.description}</p>

                <p className="flex items-center">{moment(item.date).format('L')}</p>

                <div className="flex h-7 justify-between">
                    <Rating/>
                    <HeartIcon color={user?.favorites.find(i => i.id === item.id) ? "red" : "white"} onClick={() => addFavorites(id)} className="hover:scale-125 cursor-pointer transition-all duration-150 ease-out " />
                </div>
                {session, user?.role === "admin" &&

                    <TrashIcon className="h-7 flex cursor-pointer  " onClick={() => onDelete(id)} />
                }


            </div>




        </div>
                       
                    </div>
                )
            })}
            </div>
        </div>
    )
}

export default OneCategory

export async function getServerSideProps (context) {
    const resp = await api.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/category");

if(context.query.id) {
    return {
        props: {
            category : await getCategory(context.query.id),
            categories: resp.data
        }
    } 
}
return {
    notFound: true
}


}


//choper l'id de ma catégorie