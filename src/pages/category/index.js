import React from 'react'
import CategoryList from '../../components/CategoryList'
import Header from '../../components/Header'
import api from '../../services/token-interceptors'

function Categories({ categories }) {
    return (
        <div>
            <Header />

            <CategoryList category={categories} />


        </div>
    )
}

export default Categories

export async function getServerSideProps() {
    const resp = await api.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/category");

    return {
        props: {
            categories: resp.data
        }
    }
}