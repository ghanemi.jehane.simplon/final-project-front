import { getSession, useSession } from 'next-auth/react';
import React from 'react'
import Header from '../../components/Header'
import api from '../../services/token-interceptors';

function Users({ users }) {
    const { data: session } = useSession()
    const user = session?.user
    return (
        <div>
            <Header />
            <h1 className="flex justify-center my-10 text-2xl font-medium sm:text-3xl">Welcome to users admin</h1>
            {session, user?.role === "admin" &&
                <div class="container mx-auto px-4 sm:px-8 max-w-3xl">
                    <div class="py-8">
                        <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                            <div class="inline-block min-w-full shadow rounded-lg overflow-hidden">
                                <table class="min-w-full leading-normal">
                                    <thead>
                                        <tr>
                                            <th scope="col" class="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                                Username
                                            </th>
                                            <th scope="col" class="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                                Role
                                            </th>
                                            <th scope="col" class="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                                Email
                                            </th>
                                            <th scope="col" class="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                            </th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        {users.map((item) => (


                                            <tr>
                                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                                    <div class="flex items-center">
                                                        <div class="flex-shrink-0">
                                                            <a href="#" class="block relative">
                                                                <img alt="profil" src={item.photo} class="mx-auto object-cover rounded-full h-10 w-10 " />
                                                            </a>
                                                        </div>
                                                        <div class="ml-3">
                                                            <p class="text-gray-900 whitespace-no-wrap">
                                                                {item.username}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                                    <p class="text-gray-900 whitespace-no-wrap">
                                                        {item.role}
                                                    </p>
                                                </td>
                                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                                    <p class="text-gray-900 whitespace-no-wrap">
                                                        {item.email}
                                                    </p>
                                                </td>

                                                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                                    <a href="#" class="text-indigo-600 hover:text-indigo-900">
                                                        Edit
                                                    </a>
                                                </td>
                                            </tr>

                                        ))}

                                    </tbody>

                                </table>
                             
                            </div>
                        </div>
                    </div>
                </div>
            }
        </div>
    )
}

export default Users

export async function getServerSideProps(context) {
    try {
        const session = await getSession(context)
        const response = await api.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/user", {
            headers: {
                'Authorization': 'bearer ' + session.accessToken
            }
        });

        return {
            props: {
                users: response.data
            }
        }
    } catch (error) {
        console.log(error);
        return {
            redirect : {
                destination: "/"
            },
            props: {}
        }
    }




}