import axios from 'axios';
import React, { useState } from 'react'
import Header from '../../components/Header';
import PostForm from '../../components/PostForm'
import api from '../../services/axios';
import Link from 'next/link';

function AdminPage({ articles }) {
  
    return (
        <div>
            <Header/>
            <h1 className="flex justify-center my-10 text-2xl font-medium sm:text-3xl">Admin Page</h1>

            
           
            <div className="container grid-cols-1 sm:grid-cols-3 flex justify-around">
            <Link href="/admin/users">
                <div class="relative cursor-pointer w-64 px-8 py-16 text-center text-white bg-blue-600 rounded-lg shadow-2xl">
                    <p class="text-2xl font-medium sm:text-3xl">Users</p>
                    <button class="mt-4 text-sm text-blue-100">Gérer</button>
                </div>
                 </Link>
                <Link href="/admin/articles">
                <div class="relative cursor-pointer w-64 px-8 py-16 text-center text-white bg-blue-600 rounded-lg shadow-2xl">
                    <p class="text-2xl font-medium sm:text-3xl">Articles</p>
                    <button class="mt-4 text-sm text-blue-100">Gérer</button>
                </div>
                </Link>
                <Link href="/user/account">
                <div class="relative cursor-pointer w-64 px-8 py-16 text-center text-white bg-blue-600 rounded-lg shadow-2xl">
                    <p class="text-2xl font-medium sm:text-3xl">Mon compte</p>
                    <button class="mt-4 text-sm text-blue-100">Gérer</button>
                </div>
                </Link>
            </div>

        </div>
    )
}

export default AdminPage
