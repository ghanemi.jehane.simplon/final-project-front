import React from 'react'

function UserPage() {
    return (
        <div>
            
        </div>
    )
}

export default UserPage

export async function getServerSideProps(){
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/user");
    const resp = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/category");
  
    return {
        props: {
            articles: response.data,
            categories: resp.data
        }
    }
  }