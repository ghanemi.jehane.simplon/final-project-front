import { ChevronRightIcon, UserCircleIcon } from '@heroicons/react/outline';
import moment from 'moment';
import { getSession, signIn, useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import Header from '../../components/Header'
import api from '../../services/token-interceptors';
import Link from 'next/link';

function Account({ users }) {

    const { data: session } = useSession()
    const user = session?.user;
    const router = useRouter();

    console.log(users)
    console.log(session)
    return (
        <div>
            <Header />
            {users &&

                <section class="h-screen bg-opacity-50">
                    <form class="container max-w-2xl mx-auto shadow-md md:w-3/4">
                        <div class="p-4 bg-white border-t-2 border-indigo-400 rounded-lg bg-opacity-5">
                            <div class="max-w-sm mx-auto md:w-full md:mx-0">
                                <div class="inline-flex items-center space-x-4">
                                    {user?.photo ?
                                        <a href="#" class="block relative">
                                            <img alt="profil" src={users.photo} class="mx-auto object-cover rounded-full h-16 w-16 " />
                                        </a>
                                        :
                                        <UserCircleIcon className="h-8"/>
} 
                                    <h1 class="text-white text-2xl capitalize font-semibold">
                                        {users.username}
                                    </h1>
                                </div>
                            </div>
                        </div>
                        <div class="space-y-6 bg-white">
                            <div class="items-center w-full p-8 space-y-4 text-gray-500 md:inline-flex md:space-y-0">
                                <h2 class="max-w-sm mx-auto md:w-4/12">
                                    Email
                                </h2>
                                <div class="w-full max-w-sm pl-2 mx-auto space-y-5 md:w-5/12 md:pl-9 md:inline-flex">
                                    <div class=" relative ">
                                        <input type="email" disabled id="user-info-email" class=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder={users.email} />
                                    </div>
                                </div>
                                <div class="text-center md:w-3/12 md:pl-6">
                                    <button type="button" class="py-2 px-4  bg-pink-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                                        Change
                                    </button>
                                </div>
                            </div>
                            <hr />
                            <div class="items-center w-full p-8 space-y-4 text-gray-500 md:inline-flex md:space-y-0">
                                <h2 class="max-w-sm mx-auto md:w-4/12">
                                    Username
                                </h2>
                                <div class="w-full max-w-sm pl-2 mx-auto space-y-5 md:w-5/12 md:pl-9 md:inline-flex">
                                    <div class=" relative ">
                                        <input type="text" id="user-info-password" disabled class=" capitalize rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder={users.username} />
                                    </div>
                                </div>
                                <div class="text-center md:w-3/12 md:pl-6">
                                    <button type="button" class="py-2 px-4  bg-pink-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                                        Change
                                    </button>
                                </div>
                            </div>
                            <hr />
                            <div class="items-center w-full p-8 space-y-4 text-gray-500 md:inline-flex md:space-y-0">
                                <h2 class="max-w-sm mx-auto md:w-4/12">
                                    Change password
                                </h2>
                                <div class="w-full max-w-sm pl-2 mx-auto space-y-5 md:w-5/12 md:pl-9 md:inline-flex">
                                    <div class=" relative ">
                                        <input type="text" id="user-info-password" class=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Password" />
                                    </div>
                                </div>
                                <div class="text-center md:w-3/12 md:pl-6">
                                    <button type="button" class="py-2 px-4  bg-pink-600 hover:bg-pink-700 focus:ring-pink-500 focus:ring-offset-pink-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                                        Change
                                    </button>
                                </div>
                            </div>
                            <hr />
                            <div class="">
                                <h2 class="max-w-sm mx-auto md:w-4/12 text-gray-500 p-8">
                                    Favorites
                                </h2>
                                {users.favorites.map((likes) => (
                                    <div className="container flex flex-col mx-auto w-full items-center justify-center">
                                      
                                        <ul className="flex flex-col">
                                            <Link href={"/article/" + likes.id }>
                                            <li className="flex flex-row border-gray-400 mb-2">
                                                <div className="transition duration-500 shadow ease-in-out transform hover:translate-y-1 hover:shadow-lg select-none cursor-pointer bg-white rounded-md flex flex-1 items-center p-4">
                                                    <div className="flex flex-col w-10 h-10 justify-center items-center mr-4">
                                                        <img src={likes.image} className="mx-auto object-cover rounded-md h-12 w-12" alt="" />
                                                    </div>

                                                    <div className="flex-1 pl-1 md:mr-16 text-gray-600 capitalize">
                                                        <div className="font-medium">
                                                            {likes.title}
                                                        </div>
                                                        <div className="text-sm  text-gray-600">
                                                            <p className="truncate ...">{likes.description}</p>
                                                        </div>
                                                        <div className="text-gray-600 text-xs">
                                                            <p>{moment(likes.date).format('L')}</p>
                                                        </div>


                                                    </div>
                                                    <button className="w-24 text-right flex justify-end">
                                                        <ChevronRightIcon className="h-7 w-7 hover:text-gray-800 text-gray-500" />
                                                    </button>
                                                </div>
                                            </li>
                                            </Link>
                                        </ul>
                                    </div>
                                ))}

                            </div>
                            <hr />
                            <div class="w-full px-4 pb-4 ml-auto text-gray-500 md:w-1/3">
                                <button type="submit" class="py-2 px-4  bg-blue-600 hover:bg-blue-700 focus:ring-blue-500 focus:ring-offset-blue-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </section>

            }
        </div>

    )
}

export default Account


export async function getServerSideProps(context) {
    const session = await getSession({ ctx: context })
    const user = session?.user;
    try {
        const res = await api.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/user/account", {
            headers: { 'Authorization': 'bearer ' + session.accessToken }
        })


        return {
            props: {
                users: res.data,

            }
        };
    } catch (err) {
        console.log(err)
        return {
            props: {
                users: null
            }

        }
    }


}

{/* <div>
<Header />

{users &&
    <div>
        <p>{users.id}</p>
        <p>{users.username}</p>
        <img className='rounded-full object-cover w-10 h-10 mb-1 group-hover:animate-bounce' src={users.photo} />
        <p>{users.id}</p>
    </div>
}
{!session &&
    <p>You have to log in</p>
}
</div> */}

// <div className="container ">
//                 <div className="flex justify-center ">
//                     <img src={users.photo} className="h-48 w-48 rounded-full object-cover flex justify-center" alt="" />
//                 </div>
//                 <div className="bg-white rounded-lg shadow-lg w-1/2 mx-auto space-y-5 px-auto">
//                 <div className="text-gray-700 font-semibold text-xl capitalize flex justify-center">
//                     <p>{users.username}</p>
//                 </div>
//                 <div className="flex items-center text-gray-500">
//                     <p className="text-gray-500">Email : </p>
//                     <p className="text-gray-500">{users.email}</p>
//                 </div>
//                 <div className="flex items-center">
//                     <p>Password : </p>
//                     <button className="bg-white text-blue-900 font-semibold uppercase">Modifier mon mot de passe</button>
//                 </div>
//                 </div>


//                 <div>
//                     <h2 className="items-center">Liste des favoris</h2>

//                 </div>
//             </div>