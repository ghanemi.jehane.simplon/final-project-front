import axios from 'axios'
import { getToken } from 'next-auth/jwt'
import { getSession } from 'next-auth/react'
import React from 'react'
import Header from '../../components/Header'
import api from '../../services/token-interceptors'

function OneUser({ user }) {
    console.log(user)
    return (
        <div>
            <Header/>
            {/* Ajouter condition d'accès que pour l'admin ou le user connecté */}
            {user &&
            <div>
                <p>{user.id}</p>
                <p>{user.username}</p>
                <p>{user.photo}</p>
                
            </div>
            }
        </div>
    )
}

export default OneUser

export async function getServerSideProps(context) {
    const session = await getSession({ctx:context})
    const user = session?.user;
    try {
        const id = context.query.id
        const res = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/user/" + id, {
            headers : { 'Authorization': 'bearer ' + session.accessToken }
        })
    


        return {
            props: {
                user: res.data
            }
        };
    } catch (err) {
        console.log(err)
        return {
            props: {
                user: null
            }

        }
    }

}