import axios from 'axios';
import React, { useState } from 'react'
import ArticleFeed from '../../components/ArticleFeed';
import Header from '../../components/Header';

function ArticlePage({articles, categories}) {

   


    return (
        <div>
             <Header/>
            <ArticleFeed articles={articles} />
            
        </div>
    )
}

export default ArticlePage


export async function getServerSideProps(){
    const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/article");
    const resp = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/category");
  
    return {
        props: {
            articles: response.data,
            categories: resp.data
        }
    }
  }