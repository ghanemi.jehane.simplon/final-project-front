import axios from 'axios';
import React from 'react'
import CommentsAdd from '../../components/CommentsAdd';
import Header from '../../components/Header';
import Image from 'next/image';
import moment from 'moment'
import Rating from '../../components/Rating';
import { HeartIcon, UserIcon } from '@heroicons/react/solid';
import CommentFeed from '../../components/CommentFeed';
import { addArticleFavorite } from '../../services/article-service';
import { useSession } from 'next-auth/react';
import CommentList from '../../components/CommentList';

function OneArticle({ articles, comments }) {
    console.log(articles)
    const { data: session } = useSession()
    const user = session?.user
    return (
        <div>
            <Header />
            {articles &&
                <div>

                    {/* 
                    <div className="flex flex-col-2">
                        <div className=" relative h-96 w-96 mx-20">
                            <Image src={articles.image} layout="fill" />
                        </div>
                        <div>
                            <p className="uppercase text-xl">{articles.title}</p>
                            <p>{articles.description}</p>
                            <p className="flex items-center">{moment(articles.date).format('ll')}</p>
                            <Rating />
                            <HeartIcon className="hover:scale-125  h-7 cursor-pointer
                transition-all duration-150 ease-out" />
                        </div>
                    </div> */}

                    <section>
                        <div class="max-w-screen-xl px-4 py-16 mx-auto sm:px-6 lg:px-8">
                            <div class="grid grid-cols-1  lg:grid-cols-2 gap-y-8 gap-x-16 lg:h-screen lg:items-center">
                                <a
                                    class="block overflow-hidden group"
                                    href="/products/basic-tee">
                                    <div class="shadow-[0_4px_0_0_rgba(0,0,0,1)] border-2 border-black rounded-3xl overflow-hidden">
                                        <img
                                            class="group-hover:scale-105 transition-transform duration-500 object-cover w-full h-[350px] sm:h-[450px]"
                                            src={articles.image}
                                            alt="Basic Tee Product"
                                        />
                                    </div>

                                </a>
                                <div class="max-w-lg mx-auto text-center lg:text-right lg:py-24">
                                    <h2 class="text-3xl font-bold capitalize text-gray-900 sm:text-4xl">
                                        {articles.title}
                                    </h2>

                                    <p class="mt-4 text-gray-500">
                                        {articles.description}
                                    </p>

                                    <Rating />
                                </div>


                            </div>
                        </div>
                    </section>





                    {/* Comments */}
                    {articles, user &&
                        <div className="flex flex-col p-10 my-10 bg-[#004058] shadow-lg rounded-lg ">
                            <h3 className="text-3xl text-white">Comments</h3>
                            <hr className="pb-2" />
                            {articles.comments.map((comment) => (

                                <div className="flex items-center px-4 py-6">
                                    {comment.user?.photo &&
                                        <img src={comment.user?.photo} className="rounded-full object-cover h-12 w-12" alt="" />
                                    }

                                    {comment.user?.photo == null &&
                                        <UserIcon />
                                    }


                                    <div className="space-x-4">

                                        <h2 className="text-lg font-semibold text-gray-900 mt-1 capitalize">{comment.user?.username}</h2>
                                        <p>{comment.description}</p>
                                        <p>{moment(comment.date).format('L')}</p>


                                    </div>

                                    {/* {console.log(comment.user)} */}
                                </div>

                            ))}
                        </div>
                    }

                    <CommentsAdd id={articles.id} />


                </div>

            }


        </div>
    )
}

export default OneArticle


export async function getServerSideProps(context) {
    try {
        const id = context.query.id
        const res = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/article/" + id)


        return {
            props: {
                articles: res.data
            }
        };
    } catch (err) {
        console.log(err)
        return {
            props: {
                articles: null
            }

        }
    }

}
// export async function getServerSideProps(context) {
//     const response = await axios.get(process.env.NEXT_PUBLIC_SERVER_URL + "/api/article");
//     const pageId = context.query.id
//     console.log(pageId);
//     return{
//         props: {
//             articles: pageId
//         }
//     }
// }


// export const GetServerSideProps = async (context) => {

//     if (context.query.id) {
//         return {
//             props: {
//                 articles: await fetchArticleById(context.query.id)
//             }
//         }
//     } return {
//         notFound: true
//     }

// }
